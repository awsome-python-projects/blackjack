import random
import os

print('''
These are the some rules that we will keep in mind while playing the 'BlackJack' game! Rules are,
    1. The deck is unlimited in size.
    2. There are no jokers.
    3. The Jack/Queen/King all count as 10.
    4. The Ace can count as 11 or 1.
    5. The cards in the list have equal probability of being drawn.
    6. Cards are not removed from the deck as they are drawn.
    7. The computer is the dealer.
''')

from blackjack_dependencies import deck

comp_again = False
play_again = False

dick = {}
cards = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]

# Function to clear the console screen
def clear_screen():
    if os.name == 'posix':  # for Mac and Linux
        _ = os.system('clear')
    else:  # for Windows
        _ = os.system('cls')

def random_cards():
    '''Return two random cards from a deck to each player of the game.'''
    random.shuffle(cards)
    dick['user'] = [random.choice(cards) for _ in range(2)]
    dick['computer'] = [random.choice(cards) for _ in range(2)]
    return dick

def play_ag():
    '''Pick one more card for the user.'''
    if play_again:
        card = random.choice(cards)
        dick["user"].append(card)
        return dick["user"][-1]
    
def com_ag():
    '''Pick one more card for the computer.'''
    if comp_again:
        card = random.choice(cards)
        dick["computer"].append(card)
        return dick["computer"][-1]

dicks = random_cards()
user_picks = dicks['user']
computer_picks = dicks['computer']

def scores_cal(cards):
    '''Returns the final calculated scores for user and computer.'''
    if sum(cards) > 21 and 11 in cards:
        index_11 = cards.index(11) 
        cards[index_11] = 1
    if sum(cards) == 21:
        return 21
    return sum(cards)

def update_user_score():
    '''Updates the user score and make it global variable.'''
    global user_score
    user_score = scores_cal(user_picks)
    return user_score

def update_computer_score():
    '''Updates the computer score and make it global variable.'''
    global computer_score
    computer_score = scores_cal(computer_picks)
    return computer_score

def compare_score(user, computer):
    '''Compare user and computer scores.'''
    if user_score > 21 and computer_score > 21:
        return "\nOops! You both went overboard. You lose, but hey, at least you're in good company! 😜"

    if update_user_score() == update_computer_score():
        return "\nIt's a tie! Time for a dance-off to settle this 🕺💃"
    elif update_computer_score() == 21:
        return "\nOh no, your opponent pulled a sneaky Blackjack move! Better luck next time, champ! 🃏😱"
    elif update_user_score() == 21:
        return "\nJackpot! You hit Blackjack! Time to do your victory dance! 💰😎"
    elif update_user_score() > 21:
        return "\nBusted! You went over. It happens to the best of us. Chin up! 😅😭"
    elif update_computer_score() > 21:
        return "\nHaha! Your opponent slipped up and went over. Victory is yours! 🎉😁"
    elif update_user_score() > update_computer_score():
        return "\nWinner winner, chicken dinner! You outplayed them like a pro! 🏆😃"
    else:
        return "\nOh no, tough luck! Better luck next time, but hey, it's all in good fun! 😅😤"

def blackjack():
    '''Should be 21'''
    global comp_again, play_again
    print("Your cards are:")
    for cardz in user_picks:
        if cardz == 10:
            chosen_key = random.choice([10, 20, 30, 40])
            print(deck[chosen_key])
        else:
            chosen_key = cardz
            print(deck[chosen_key])
    print(f"Card values are: {user_picks}")
    print(f"Current score: {update_user_score()}!")

    if computer_picks[0] == 10:
        chosen_key = random.choice([10, 20, 30, 40])
    else:
        chosen_key = computer_picks[0]

    print(f"\nDealer card is: {deck[chosen_key]}")
    print(f"Card value: {computer_picks[0]}")

    game_over = False

    while not game_over:
        if update_user_score() == 0 or update_computer_score() == 0 or update_user_score() > 21:
            print(f"Uh-oh! You overshot 21 by {update_user_score() - 21}. Looks like the game had other plans for you! Game over, better luck next time! 🚀😅")
            game_over = True
        else:
            again_play = input('''Do you want to re-suffle the cards again and get a another card? Press,
            1. 'Y' for Yes
            2. 'N' for No\n''').lower()

            if again_play == 'y':
                play_again = True
                new_card = play_ag()
                update_user_score()

                print("\nYour new card is:")
                if new_card == 10:
                    chosen_key = random.choice([10, 20, 30, 40])
                elif 1 in user_picks:
                    chosen_key = 11
                    update_user_score()
                else:
                    chosen_key = new_card

                print(deck[chosen_key])
                print(f"Updated cards are: {user_picks}")
                print(f"Current score: {update_user_score()}!")

            elif again_play == 'n':
                game_over = True

    while update_computer_score() != 0 and update_computer_score() < 17:
        comp_again = True
        new_cards = com_ag()
        update_computer_score()

        if new_cards == 10:
            chosen_keyz = random.choice([10, 20, 30, 40])
            index10 = computer_picks.index(10)
            computer_picks[index10] = chosen_keyz
        elif 1 in computer_picks:
            index1_u = user_picks.index(1)
            computer_picks[index1_u] = 11
            update_computer_score()

    print("\nDealer's final hand:")
    for cardzz in computer_picks:
        print(deck[cardzz])

    print(computer_picks)
    print(f"Dealer's total Score: {update_computer_score()}\n")

    print(f"\nYour final hand:")
    if 1 in user_picks:
        user_picks.remove(1)
        user_picks.append(11)
    for cadzz in user_picks:
        print(deck[cadzz])

    print(user_picks)
    print(f"Your total score: {update_user_score()}")

    print("\nAfter some serious card shuffling and a bit of magic, here's the verdict:")
    print(f"\tYou picked these gems: {user_picks} and racked up a score of {update_user_score()}. Not bad, not bad at all! 🃏🎉")
    print(f"\tThe dealer, on the other hand, went for: {computer_picks} and ended up with a score of {update_computer_score()}. 🎩🔮")

    print(compare_score(update_user_score(), update_computer_score()))

    replay = input("Hey there! Do you want to continue this game? Press:\n1. 'Y' for Yes\n2. 'N' for No\n").lower()
    if replay == 'y':
        clear_screen()
        blackjack()
    else:
        print("Thanks for playing! Have a great day!")  

blackjack()