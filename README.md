# BlackJack Game

Welcome to the BlackJack game! Here are the rules you need to know:

1. **Deck**: The deck is unlimited in size.
2. **Jokers**: There are no jokers.
3. **Face Cards**: The Jack, Queen, and King all count as 10.
4. **Ace**: The Ace can count as either 11 or 1, depending on the player's choice.
5. **Card Probability**: All cards in the deck have equal probability of being drawn.
6. **Card Removal**: Cards are not removed from the deck as they are drawn.
7. **Dealer**: The computer acts as the dealer.

## How to Play

1. **Objective**: The goal is to get a hand value as close to 21 as possible without exceeding it.
2. **Initial Deal**: Each player (including the dealer) receives two cards.
3. **Player's Turn**:
   - If your hand value exceeds 21, you lose.
   - If you get exactly 21 with your first two cards (an Ace and a 10-value card), you have a "BlackJack."
4. **Dealer's Turn**:
   - The dealer reveals their facedown card.
   - The dealer must hit until their hand value is at least 17.
5. **Winning Conditions**:
   - If your hand value is higher than the dealer's without exceeding 21, you win.
   - If the dealer's hand value exceeds 21, you win.
   - If both you and the dealer have the same hand value, it's a push (tie).

## Running the Game

1. Clone this repository.
2. Open the terminal/command prompt and navigate to the project folder.
3. Run the game using the command: `python blackjack.py`.

Have fun playing BlackJack!
